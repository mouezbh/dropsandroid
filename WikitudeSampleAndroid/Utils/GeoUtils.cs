
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Hardware;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Json;
using Android.Locations;

namespace Com.Wikitude.Samples
{
    public class GeoUtils
    {
        private static readonly DateTime Jan1st1970 = new DateTime
       (1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long CurrentTimeMillis()
        {
            return (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
        }
        public static JsonArray GetPoiInformation(Location userLocation,float mAzimuth,float inclination)
        {
            if (userLocation == null)
                return null;

            var pois = new List<JsonObject>();
            
            {
                //GeomagneticField geoField = new GeomagneticField(
                //(float)userLocation.Latitude,
                //(float)userLocation.Longitude,
                //(float)userLocation.Altitude,
                //CurrentTimeMillis());
                //mAzimuth = (int)(mAzimuth + geoField.Declination);//+ 360) % 360;
              //  Console.WriteLine(inclination + "44444444444");

               // Console.WriteLine(mAzimuth+"5555555555");
                var loc = GetLatLonNearby(userLocation.Latitude, userLocation.Longitude, userLocation.Altitude,20, mAzimuth, inclination);
                Console.WriteLine(userLocation.Latitude+","+ userLocation.Longitude+","+ userLocation.Altitude+"88888888" + loc[0]+","+loc[1]+","+loc[2]);
                var p = new Dictionary<string, JsonValue>(){
                    { "id","1" },
                    { "name", "POI#+1"  },
                    { "description", "This is the description of POI#"  },
                    { "latitude", loc[0] },
                    { "longitude", loc[1] },
                    { "altitude",loc[2] }
                };


                pois.Add(new JsonObject(p.ToList()));
            }

            var vals = from p in pois select (JsonValue)p;

            return new JsonArray(vals);
        }

        static double[] GetLatLonNearby(double lat, double lon,double al,double distance,float mazimuth,float incli)
        {
            var inclin =distance * Math.Tan(incli- (Math.PI/2));
          //  Console.WriteLine(incli + "pppppppppp");
           // Console.WriteLine(distance + "ettttttttttte");
           // Console.WriteLine(Math.Tan(incli) + "eeeeeeeeeeeeee");
            double  EarthRadius = 6371000;
            var azimuth = mazimuth*Math.PI / 180;
            var b = distance / EarthRadius;
            var a = Math.Acos(Math.Cos(b) * Math.Cos((90 - lat) * Math.PI / 180) + Math.Sin((90 - lat) * Math.PI / 180) * Math.Sin(b) * Math.Cos(azimuth));
            var B = Math.Asin(Math.Sin(b) * Math.Sin(azimuth) / Math.Sin(a));

            var Lat2 = 90 - (a * 180 / Math.PI);
            var Lon2 = (B * 180 / Math.PI) + lon;
            var Al2 = al+ inclin;
            return new double[] { Math.Round(Lat2, 5), Math.Round(Lon2, 5), Math.Round(Al2, 5) };

            /*    double azimuth = mazimuth * Math.PI / 180;
                double b = distance / 6371000;
                double a = Math.Acos(Math.Cos(b) * Math.Cos((90 - lat) *  Math.PI/180) + Math.Sin((90 - lat) * Math.PI / 180) * Math.Sin(b) * Math.Cos(azimuth));
                double c = Math.Asin(Math.Sin(b) * Math.Sin(azimuth) / Math.Sin(a));
                var newLat = 90- (a * 180 / Math.PI);
                var newLng = (c * 180 / Math.PI) + lon;

                return new double[] { newLat, newLng };*/
        }
    }
}


